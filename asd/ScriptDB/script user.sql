CREATE USER 'Alessandro'@localhost IDENTIFIED BY 'capaebello99';
GRANT SELECT,INSERT,UPDATE ON *.* TO 'Alessandro'@localhost;

CREATE USER 'Federico'@localhost IDENTIFIED BY 'fedemane99';
GRANT SELECT,INSERT,UPDATE ON *.* TO 'Federico'@localhost;

CREATE USER 'Application'@localhost IDENTIFIED BY 'appgioco';
GRANT SELECT,INSERT,UPDATE ON *.* TO 'Application'@localhost;

CREATE USER 'Stefano'@localhost IDENTIFIED BY 'hofattoloscript';
GRANT SELECT,INSERT,UPDATE ON *.* TO 'Stefano'@localhost;
