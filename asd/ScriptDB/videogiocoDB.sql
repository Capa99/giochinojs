-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Creato il: Dic 14, 2018 alle 12:31
-- Versione del server: 5.7.23
-- Versione PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `videogioco`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `gioco`
--

DROP TABLE IF EXISTS `gioco`;
CREATE TABLE IF NOT EXISTS `gioco` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(15) NOT NULL,
  `Difficolta` int(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `gioco`
--

INSERT INTO `gioco` (`ID`, `Nome`, `Difficolta`) VALUES
(1, 'Bioshock ', 2),
(2, 'Pokemon', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `partita`
--

DROP TABLE IF EXISTS `partita`;
CREATE TABLE IF NOT EXISTS `partita` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `IDUtente` int(5) NOT NULL,
  `IDGioco` int(5) NOT NULL,
  `Punteggio` int(5) NOT NULL,
  `Data` date NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `partita_ibfk_1` (`IDUtente`),
  KEY `partita_ibfk_2` (`IDGioco`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `partita`
--

INSERT INTO `partita` (`ID`, `IDUtente`, `IDGioco`, `Punteggio`, `Data`) VALUES
(2, 2, 2, 2000, '2018-11-06'),
(3, 3, 1, 5000, '2018-01-09'),
(4, 3, 2, 5000, '2018-03-03');

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

DROP TABLE IF EXISTS `utente`;
CREATE TABLE IF NOT EXISTS `utente` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(15) NOT NULL,
  `Cognome` varchar(15) NOT NULL,
  `Nickname` varchar(15) NOT NULL,
  `Password` varchar(15) NOT NULL,
  `Paese` varchar(15) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`ID`, `Nome`, `Cognome`, `Nickname`, `Password`, `Paese`) VALUES
(1, '0', '0', 'Federico', 'fedemane99', 'Vigevano'),
(2, '0', '0', 'Giovanni', '12345', 'Germania'),
(3, '0', '0', 'Pancrazio', 'Pan123', 'Austria'),
(5, '0', '0', 'Pippo12345', 'Pippo12345!', 'Barbados');

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `partita`
--
ALTER TABLE `partita`
  ADD CONSTRAINT `partita_ibfk_1` FOREIGN KEY (`IDUtente`) REFERENCES `utente` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `partita_ibfk_2` FOREIGN KEY (`IDGioco`) REFERENCES `gioco` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
