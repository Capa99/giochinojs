<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/styles.css">
<style>
#centro { 
margin: 0 auto; 
width:250px;
}
.container{
    width: 100%;
    text-align: center;
}
.container form{
    display: inline-block;
}
#foto{
	width: 100%;
    text-align: center;
}
#s{
	width:500x;
	height:500px;
}
#f{
	width:500px;
	height:500px;
}
#a{
	width:500px;
	height:500px;
}
</style>
  </head>
<body id="body">

	<h2><% if(session.getAttribute("user")!=null){%>Benvenuto ${user.nickname} da ${user.paese} !<%} %></h2>
	<h2>${messaggio}</h2>
	<h2>${salvato}  ${punteggio}</h2>
	<p id="centro">
		<img id="logo" alt="gif" src="img2/gioco.gif">
	</p>
  <div id="buttonindex" class="container">
  <form method="get" action="login">
  <input type="submit" class="btn btn-dark btn-lg" value="LOGIN">
  </form>
  <form method="get" action="registra">
  <input type="submit" class="btn btn-dark btn-lg" value="REGISTRATI">
  </form>
  <form method="get" action="classifica">
  <input type="submit" class="btn btn-dark btn-lg" value="VAI A CLASSIFICA">
  </form>
  <form method="get" action="gioco">
  <input type="submit" class="btn btn-dark btn-lg" value="INIZIA A GIOCARE">
  </form>
  <form method="get" action="modifica">
  <input type="submit" class="btn btn-dark btn-lg" value="MODIFICA DATI">
  </form>
  <form method="get" action="logout" id="btn1">
<input type="submit" class="btn btn-dark btn-lg" value="LOGOUT">
</form>
  </div>
  <br>
  <br>
  <div id ="foto">
  <img id="s" alt="jpg" src="img2/stefanoindex.jpg">
  <img id="f" alt="jpg" src="img2/federicoindex.jpg">
  <img id="a" alt="jpg" src="img2/alessandroindex.jpg">
  
  </div>
  
  </body>
</html>