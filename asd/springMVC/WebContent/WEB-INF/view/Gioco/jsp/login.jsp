<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/form.js"></script>
  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.5.0/css/all.css' integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU' crossorigin='anonymous'>
  
  
  
  <script>
  function mostraPassword() {
	     var temp = document.getElementById("password"); 
	      
	            if (temp.type === "password" ) { 
	                temp.type = "text"; 
	                pass.classList.add("fa-eye-slash");
	                pass.classList.remove("fa-eye");
	            } 
	            else { 
	                temp.type = "password"; 
	                   pass.classList.remove("fa-eye-slash");
	                 pass.classList.add("fa-eye");
	            } 
	        } 

  </script>
  <style>
  
  
  input:hover {
    background-color: #fff999;

}
input[type="password"]:focus:invalid  {
  outline: 2px solid #ff4747;
 background-color: #ff4747;
}



input:focus:valid {
 outline: 2px solid #6dff54;
 background-color: #6dff54	;
 }
 
#centro { 
	margin: 0 auto; 
	width:250px;
}
#cont1{
    width: 100%;
    text-align: center;
}

#btn1{
    float:left;
}
#btn3{
    float:right;
}
  </style>
</head>
<body>
<br>

<div class="container" id="cont1">
<form method="get" action="index" id="btn1">
<input type="submit" class="btn btn-dark btn-lg" value="HOME">
</form>
<form method="get" action="modifica" id="btn3">
<input type="submit" class="btn btn-dark btn-lg" value="MODIFICA DATI">
</form>
</div>
<p id="centro">
		<img id="logo" alt="gif" src="img2/gioco.gif">
	</p>
<div class="container">
<br>
	
  <h2>Accedi per giocare!</h2>
  <br>
  <label text-align="center">${messaggio}</label>
  <br>
  <form method="GET" action="login2">
    <div class="form-group">
      <label  >Nickname:</label>
      <input  class="form-control" id="nickname" placeholder="Inserisci nickname" name="nickname" required/>
    </div>
    <div class="form-group">
      <label>Password:</label>
      <div class="input-group mb-3">
      <input  type="password" class="form-control" id="password" placeholder="Inserisci password" name="password" data-toggle="password" required/>
      <div class="input-group-append">
      	<button type="button" onclick="mostraPassword()"><i id="password" class='far fa-eye-slash'></i></button>
      	</div>
      	</div>
    </div>
    <br>
    <button type="submit" class="btn btn-dark">Invia</button>
  </form>
</div>
<br>
</body>
</html>
