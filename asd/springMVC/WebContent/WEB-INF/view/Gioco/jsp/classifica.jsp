<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
  
  <link rel="stylesheet" href="css/styles.css">
<script type="text/javascript">
angular.module("myApp", [])
 
   
   .controller("userController0", function($scope,$http) { 
	   $scope.paese="${u0.utente.paese}";
      $http.get("https://restcountries.eu/rest/v2/name/"+$scope.paese)
          .then (
                function (contenuto) {
                	
                   $scope.dataZero = contenuto.data[0];
                });
	
   })
   
   .controller("userController1", function($scope,$http) { 
	   $scope.paese="${u1.utente.paese}";
	      $http.get("https://restcountries.eu/rest/v2/name/"+$scope.paese)
	          .then (
	                function (contenuto) {
	                	
	                   $scope.dataZero = contenuto.data[0];
	                });
		
	   })
	   
	   .controller("userController2", function($scope,$http) { 
	   $scope.paese="${u2.utente.paese}";
      $http.get("https://restcountries.eu/rest/v2/name/"+$scope.paese)
          .then (
                function (contenuto) {
                	
                   $scope.dataZero = contenuto.data[0];
                });
	
   })
   
   .controller("userController3", function($scope,$http) { 
	   $scope.paese="${u3.utente.paese}";
      $http.get("https://restcountries.eu/rest/v2/name/"+$scope.paese)
          .then (
                function (contenuto) {
                	
                   $scope.dataZero = contenuto.data[0];
                });
	
   })
   
   .controller("userController4", function($scope,$http) { 
	   $scope.paese="${u4.utente.paese}";
      $http.get("https://restcountries.eu/rest/v2/name/"+$scope.paese)
          .then (
                function (contenuto) {
                	
                   $scope.dataZero = contenuto.data[0];
                });
	
   })
   
   .controller("userController5", function($scope,$http) { 
	   $scope.paese="${u5.utente.paese}";
      $http.get("https://restcountries.eu/rest/v2/name/"+$scope.paese)
          .then (
                function (contenuto) {
                	
                   $scope.dataZero = contenuto.data[0];
                });
	
   })
   
   .controller("userController6", function($scope,$http) { 
	   $scope.paese="${u6.utente.paese}";
      $http.get("https://restcountries.eu/rest/v2/name/"+$scope.paese)
          .then (
                function (contenuto) {
                	
                   $scope.dataZero = contenuto.data[0];
                });
	
   })
   
   .controller("userController7", function($scope,$http) { 
	   $scope.paese="${u7.utente.paese}";
      $http.get("https://restcountries.eu/rest/v2/name/"+$scope.paese)
          .then (
                function (contenuto) {
                	
                   $scope.dataZero = contenuto.data[0];
                });
	
   });
   
  
</script>
<style>
#centro { 
margin: 0 auto; 
width:250px;
}
.container{
    width: 100%;
    float: left;
    text-align: center;
}
.container form{
    display: inline-block;
}
#btn1{
    float:left;
}
#btn3{
    float:right;
}
</style>
</head>
<body ng-app="myApp" >

<br>
<div class="container">
<form method="get" action="logout" id="btn1">
<input type="submit" class="btn btn-dark btn-lg" value="LOGOUT">
</form>
<form method="get" action="index">
<input type="submit" class="btn btn-dark btn-lg" value="HOME">
</form>
<form method="get" action="modifica" id="btn3">
<input type="submit" class="btn btn-dark btn-lg" value="MODIFICA DATI">
</form>
</div>
<p id="centro">
		<img id="logo" alt="gif" src="img2/gioco.gif">
	</p>
<br>
	<h1 id="classifica">CLASSIFICA</h2>
	<div class="container">

   <br>
	<table id= "table" class="table table-bordered">
   <thead>
   <tr>
		<th>Nickname</th>
		<th>Punteggio</th> 
		<th>Posizione</th>
		<th>Paese</th>
   </tr>
   </thead>
   <tbody>
   
	<tr id="primo" ng-controller="userController0" >
      <td>${u0.utente.nickname}</td>
		<td>${u0.punteggio}</td>
		<td>Primo</td> 
		<td><img src="{{dataZero.flag}}" title="{{dataZero.name}}" width="35px" height="20px"></td>		
	</tr>
	
	
	<tr id="secondo" ng-controller="userController1">
		<td>${u1.utente.nickname}</td>
		<td>${u1.punteggio}</td>
		<td>Secondo</td>
		<td><img src="{{dataZero.flag}}" title="{{dataZero.name}}" width="35px" height="20px"></td>
	</tr>
	<tr id="terzo" ng-controller="userController2">
		<td>${u2.utente.nickname}</td> 
		<td>${u2.punteggio}</td>
		<td>Terzo</td>
		<td><img src="{{dataZero.flag}}" title="{{dataZero.name}}" width="35px" height="20px"></td>
	</tr>
	<tr ng-controller="userController3">
		<td>${u3.utente.nickname}</td>
		<td>${u3.punteggio}</td>
		<td>Quarto</td>
		<td><img src="{{dataZero.flag}}" title="{{dataZero.name}}" width="35px" height="20px"></td>
	</tr>
   <tr ng-controller="userController4">
		<td>${u4.utente.nickname}</td>
		<td>${u4.punteggio}</td>
		<td>Quinto</td>
		<td><img src="{{dataZero.flag}}" title="{{dataZero.name}}" width="35px" height="20px"></td>
	</tr>
   <tr ng-controller="userController5">
		<td>${u5.utente.nickname}</td>
		<td>${u5.punteggio}</td>
		<td>Sesto</td>
		<td><img src="{{dataZero.flag}}" title="{{dataZero.name}}"width="35px" height="20px"></td>
	</tr>
   <tr ng-controller="userController6">
		<td>${u6.utente.nickname}</td>
		<td>${u6.punteggio}</td>
		<td>Settimo</td>
		<td><img src="{{dataZero.flag}}" title="{{dataZero.name}}" width="35px" height="20px"></td>
	</tr>
   <tr ng-controller="userController7">
		<td>${u7.utente.nickname}</td>
		<td>${u7.punteggio}</td>
		<td>Ottavo</td>
		<td><img src="{{dataZero.flag}}" title="{{dataZero.name}}" width="35px" height="20px"></td>
	</tr>
   </tbody>
	</table>
</div>
</body>
</html>