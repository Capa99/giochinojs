<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/styles.css">

<script type="text/javascript" src="js/mappa.js"></script>
<script type="text/javascript" src="js/movimento.js"></script>
<script type="text/javascript" src="js/funzioni.js"></script>
<script type="text/javascript" src="js/livello1.js"></script>
<script type="text/javascript" src="js/livello2.js"></script>
<script type="text/javascript" src="js/livello3.js"></script>
<script type="text/javascript" src="js/livello4.js"></script>
<script type="text/javascript" src="js/livello5.js"></script>
<script type="text/javascript" src="js/livello6.js"></script>
<script type="text/javascript" src="js/livello7.js"></script>
<script type="text/javascript" src="js/livello8.js"></script>
<script type="text/javascript" src="js/livello9.js"></script>
<script type="text/javascript" src="js/livello10.js"></script>
<style>
#centro { 
margin: 0 auto; 
width:250px;
}
.container{
    width: 100%;
    float: left;
    text-align: center;
}
.container form{
    display: inline-block;
}
#btn1{
    float:left;
}
#btn3{
    float:right;
}
</style>
</head>
<body id="body">
<br>
<div class="container">
<form method="get" action="logout" id="btn1">
<input type="submit" class="btn btn-dark btn-lg" value="LOGOUT">
</form>
<form method="get" action="index">
<input type="submit" class="btn btn-dark btn-lg" value="HOME">
</form>
<form method="get" action="modifica" id="btn3">
<input type="submit" class="btn btn-dark btn-lg" value="MODIFICA DATI">
</form>
</div>
	<p id="centro">
		<img id="logo" alt="gif" src="img2/gioco.gif">
	</p>
	<h2>Benvenuto ${user.nickname} da  ${user.paese}!</h2>
	
	<div id="centrosb">
		<p id="sfondoBarra"></p>
		<p id="barraCompletamento"class="progress-bar progress-bar-striped progress-bar-animated" ></p>
	</div>
	<br>
	<div id="centrosbarra">
		<p id="sfondosBarra"></p>
		<p id="myBar" class="progress-bar progress-bar-striped progress-bar-animated" ></p>
	</div>
	<br> <br> <br>
	<h1 id="en"></h1>
	<p id="energia">0</p>		
	<div id="det">DETTAGLI</div><br>
<div id="dettagli">	
<a> <img id="popoverostacolo" class="btn" src="img1/3.jpg" href="#" data-content="Non puoi passare sopra a questo oggetto"rel="popover" data-placement="top" data-original-title="Ostacolo" data-trigger="hover"></a>
<a> <img id="popoverspada" class="btn" src="img1/2.jpg" href="#" data-content="Ti permette di uccidere i nemici"rel="popover" data-placement="top" data-original-title="Spada" data-trigger="hover"></a>
<a> <img id="popoverpillola" class="btn" src="img1/1.jpg" href="#" data-content="Prendile tutte per superare il livello"rel="popover" data-placement="top" data-original-title="Pillola" data-trigger="hover"></a>
<a> <img id="popoverfungo" class="btn" src="img1/4.jpg" href="#" data-content="Se ci passi sopra perdi energia"rel="popover" data-placement="top" data-original-title="Fungo" data-trigger="hover"></a>
<a> <img id="popoverbuconero" class="btn" src="img1/6.jpg" href="#" data-content="Se ci passi sopra perdi istantaneamente"rel="popover" data-placement="top" data-original-title="Buconero" data-trigger="hover"></a>
<a> <img id="popoverportale" class="btn" src="img1/5.jpg" href="#" data-content="Ti permette di spostarti da un portale all'altro"rel="popover" data-placement="top" data-original-title="Portale" data-trigger="hover"></a>
<a> <img id="popovernemico1" class="btn" src="img1/7.jpg" href="#" data-content="Nemico Standard, ti insegue se ti avvicini"rel="popover" data-placement="top" data-original-title="Hunter" data-trigger="hover"></a>
<a> <img id="popovernemico2" class="btn" src="img1/8.jpg" href="#" data-content="Nemico veloce, ti insegue se ti avvicini"rel="popover" data-placement="top" data-original-title="Runner" data-trigger="hover"></a>
<a> <img id="popoveromino" class="btn" src="img1/omino.jpg" href="#" data-content="Hei! ma questo sei TU!"rel="popover" data-placement="top" data-original-title="Protagonista" data-trigger="hover"></a>
<a> <img id="popoverominoConSpada" class="btn" src="img1/ominoConSpada.jpg" href="#" data-content="Tu ma con una spada"rel="popover" data-placement="top" data-original-title="Protagonista con spada" data-trigger="hover"></a>
<a> <img id="popoverporta" class="btn" src="img1/12.jpg" href="#" data-content="Ti servir� una chiave per aprire questa porta"rel="popover" data-placement="top" data-original-title="Porta" data-trigger="hover"></a>
<a> <img id="popoverchiave" class="btn" src="img1/11.jpg" href="#" data-content="Ti permette di aprire le porte"rel="popover" data-placement="top" data-original-title="Chiave" data-trigger="hover"></a>
<a> <img id="popoverlaser" class="btn" src="img1/15.jpg" href="#" data-content="CARICATE IL LASEROOOONEE"rel="popover" data-placement="top" data-original-title="Laser" data-trigger="hover"></a>

	</div>
<br><br>
	<p id="gioco" class="col-sm-20">
		<img id="c0_0" src="img1/0.jpg">
		<img id="c0_1" src="img1/0.jpg">
		<img id="c0_2" src="img1/0.jpg">
		<img id="c0_3" src="img1/0.jpg">
		<img id="c0_4" src="img1/0.jpg">
		<img id="c0_5" src="img1/0.jpg">
		<img id="c0_6" src="img1/0.jpg">
		<img id="c0_7" src="img1/0.jpg">
		<img id="c0_8" src="img1/0.jpg">
		<img id="c0_9" src="img1/0.jpg">
		<img id="c0_10" src="img1/0.jpg">
		<img id="c0_11" src="img1/0.jpg">
		<img id="c0_12" src="img1/0.jpg">
		<img id="c0_13" src="img1/0.jpg">
		<img id="c0_14" src="img1/0.jpg">
		<img id="c0_15" src="img1/0.jpg">
		<img id="c0_16" src="img1/0.jpg">
		<img id="c0_17" src="img1/0.jpg">
		<img id="c0_18" src="img1/0.jpg">
		<img id="c0_19" src="img1/0.jpg">
		<br>
		<img id="c1_0" src="img1/0.jpg">
		<img id="c1_1" src="img1/0.jpg">
		<img id="c1_2" src="img1/0.jpg">
		<img id="c1_3" src="img1/0.jpg">
		<img id="c1_4" src="img1/0.jpg">
		<img id="c1_5" src="img1/0.jpg">
		<img id="c1_6" src="img1/0.jpg">
		<img id="c1_7" src="img1/0.jpg">
		<img id="c1_8" src="img1/0.jpg">
		<img id="c1_9" src="img1/0.jpg">
		<img id="c1_10" src="img1/0.jpg">
		<img id="c1_11" src="img1/0.jpg">
		<img id="c1_12" src="img1/0.jpg">
		<img id="c1_13" src="img1/0.jpg">
		<img id="c1_14" src="img1/0.jpg">
		<img id="c1_15" src="img1/0.jpg">
		<img id="c1_16" src="img1/0.jpg">
		<img id="c1_17" src="img1/0.jpg">
		<img id="c1_18" src="img1/0.jpg">
		<img id="c1_19" src="img1/0.jpg">
		<br>
		<img id="c2_0" src="img1/0.jpg">
		<img id="c2_1" src="img1/0.jpg">
		<img id="c2_2" src="img1/0.jpg">
		<img id="c2_3" src="img1/0.jpg">
		<img id="c2_4" src="img1/0.jpg">
		<img id="c2_5" src="img1/0.jpg">
		<img id="c2_6" src="img1/0.jpg">
		<img id="c2_7" src="img1/0.jpg">
		<img id="c2_8" src="img1/0.jpg">
		<img id="c2_9" src="img1/0.jpg">
		<img id="c2_10" src="img1/0.jpg">
		<img id="c2_11" src="img1/0.jpg">
		<img id="c2_12" src="img1/0.jpg">
		<img id="c2_13" src="img1/0.jpg">
		<img id="c2_14" src="img1/0.jpg">
		<img id="c2_15" src="img1/0.jpg">
		<img id="c2_16" src="img1/0.jpg">
		<img id="c2_17" src="img1/0.jpg">
		<img id="c2_18" src="img1/0.jpg">
		<img id="c2_19" src="img1/0.jpg">
		<br>
		<img id="c3_0" src="img1/0.jpg">
		<img id="c3_1" src="img1/0.jpg">
		<img id="c3_2" src="img1/0.jpg">
		<img id="c3_3" src="img1/0.jpg">
		<img id="c3_4" src="img1/0.jpg">
		<img id="c3_5" src="img1/0.jpg">
		<img id="c3_6" src="img1/0.jpg">
		<img id="c3_7" src="img1/0.jpg">
		<img id="c3_8" src="img1/0.jpg">
		<img id="c3_9" src="img1/0.jpg">
		<img id="c3_10" src="img1/0.jpg">
		<img id="c3_11" src="img1/0.jpg">
		<img id="c3_12" src="img1/0.jpg">
		<img id="c3_13" src="img1/0.jpg">
		<img id="c3_14" src="img1/0.jpg">
		<img id="c3_15" src="img1/0.jpg">
		<img id="c3_16" src="img1/0.jpg">
		<img id="c3_17" src="img1/0.jpg">
		<img id="c3_18" src="img1/0.jpg">
		<img id="c3_19" src="img1/0.jpg">
		<br>
		<img id="c4_0" src="img1/0.jpg">
		<img id="c4_1" src="img1/0.jpg">
		<img id="c4_2" src="img1/0.jpg">
		<img id="c4_3" src="img1/0.jpg">
		<img id="c4_4" src="img1/0.jpg">
		<img id="c4_5" src="img1/0.jpg">
		<img id="c4_6" src="img1/0.jpg">
		<img id="c4_7" src="img1/0.jpg">
		<img id="c4_8" src="img1/0.jpg">
		<img id="c4_9" src="img1/0.jpg">
		<img id="c4_10" src="img1/0.jpg">
		<img id="c4_11" src="img1/0.jpg">
		<img id="c4_12" src="img1/0.jpg">
		<img id="c4_13" src="img1/0.jpg">
		<img id="c4_14" src="img1/0.jpg">
		<img id="c4_15" src="img1/0.jpg">
		<img id="c4_16" src="img1/0.jpg">
		<img id="c4_17" src="img1/0.jpg">
		<img id="c4_18" src="img1/0.jpg">
		<img id="c4_19" src="img1/0.jpg">
		<br>
		<img id="c5_0" src="img1/0.jpg">
		<img id="c5_1" src="img1/0.jpg">
		<img id="c5_2" src="img1/0.jpg">
		<img id="c5_3" src="img1/0.jpg">
		<img id="c5_4" src="img1/0.jpg">
		<img id="c5_5" src="img1/0.jpg">
		<img id="c5_6" src="img1/0.jpg">
		<img id="c5_7" src="img1/0.jpg">
		<img id="c5_8" src="img1/0.jpg">
		<img id="c5_9" src="img1/0.jpg">
		<img id="c5_10" src="img1/0.jpg">
		<img id="c5_11" src="img1/0.jpg">
		<img id="c5_12" src="img1/0.jpg">
		<img id="c5_13" src="img1/0.jpg">
		<img id="c5_14" src="img1/0.jpg">
		<img id="c5_15" src="img1/0.jpg">
		<img id="c5_16" src="img1/0.jpg">
		<img id="c5_17" src="img1/0.jpg">
		<img id="c5_18" src="img1/0.jpg">
		<img id="c5_19" src="img1/0.jpg">
		<br>
		<img id="c6_0" src="img1/0.jpg">
		<img id="c6_1" src="img1/0.jpg">
		<img id="c6_2" src="img1/0.jpg">
		<img id="c6_3" src="img1/0.jpg">
		<img id="c6_4" src="img1/0.jpg">
		<img id="c6_5" src="img1/0.jpg">
		<img id="c6_6" src="img1/0.jpg">
		<img id="c6_7" src="img1/0.jpg">
		<img id="c6_8" src="img1/0.jpg">
		<img id="c6_9" src="img1/0.jpg">
		<img id="c6_10" src="img1/0.jpg">
		<img id="c6_11" src="img1/0.jpg">
		<img id="c6_12" src="img1/0.jpg">
		<img id="c6_13" src="img1/0.jpg">
		<img id="c6_14" src="img1/0.jpg">
		<img id="c6_15" src="img1/0.jpg">
		<img id="c6_16" src="img1/0.jpg">
		<img id="c6_17" src="img1/0.jpg">
		<img id="c6_18" src="img1/0.jpg">
		<img id="c6_19" src="img1/0.jpg">
		<br>
		<img id="c7_0" src="img1/0.jpg">
		<img id="c7_1" src="img1/0.jpg">
		<img id="c7_2" src="img1/0.jpg">
		<img id="c7_3" src="img1/0.jpg">
		<img id="c7_4" src="img1/0.jpg">
		<img id="c7_5" src="img1/0.jpg">
		<img id="c7_6" src="img1/0.jpg">
		<img id="c7_7" src="img1/0.jpg">
		<img id="c7_8" src="img1/0.jpg">
		<img id="c7_9" src="img1/0.jpg">
		<img id="c7_10" src="img1/0.jpg">
		<img id="c7_11" src="img1/0.jpg">
		<img id="c7_12" src="img1/0.jpg">
		<img id="c7_13" src="img1/0.jpg">
		<img id="c7_14" src="img1/0.jpg">
		<img id="c7_15" src="img1/0.jpg">
		<img id="c7_16" src="img1/0.jpg">
		<img id="c7_17" src="img1/0.jpg">
		<img id="c7_18" src="img1/0.jpg">
		<img id="c7_19" src="img1/0.jpg">
		<br>
		<img id="c8_0" src="img1/0.jpg">
		<img id="c8_1" src="img1/0.jpg">
		<img id="c8_2" src="img1/0.jpg">
		<img id="c8_3" src="img1/0.jpg">
		<img id="c8_4" src="img1/0.jpg">
		<img id="c8_5" src="img1/0.jpg">
		<img id="c8_6" src="img1/0.jpg">
		<img id="c8_7" src="img1/0.jpg">
		<img id="c8_8" src="img1/0.jpg">
		<img id="c8_9" src="img1/0.jpg">
		<img id="c8_10" src="img1/0.jpg">
		<img id="c8_11" src="img1/0.jpg">
		<img id="c8_12" src="img1/0.jpg">
		<img id="c8_13" src="img1/0.jpg">
		<img id="c8_14" src="img1/0.jpg">
		<img id="c8_15" src="img1/0.jpg">
		<img id="c8_16" src="img1/0.jpg">
		<img id="c8_17" src="img1/0.jpg">
		<img id="c8_18" src="img1/0.jpg">
		<img id="c8_19" src="img1/0.jpg">
		<br>
		<img id="c9_0" src="img1/0.jpg">
		<img id="c9_1" src="img1/0.jpg">
		<img id="c9_2" src="img1/0.jpg">
		<img id="c9_3" src="img1/0.jpg">
		<img id="c9_4" src="img1/0.jpg">
		<img id="c9_5" src="img1/0.jpg">
		<img id="c9_6" src="img1/0.jpg">
		<img id="c9_7" src="img1/0.jpg">
		<img id="c9_8" src="img1/0.jpg">
		<img id="c9_9" src="img1/0.jpg">
		<img id="c9_10" src="img1/0.jpg">
		<img id="c9_11" src="img1/0.jpg">
		<img id="c9_12" src="img1/0.jpg">
		<img id="c9_13" src="img1/0.jpg">
		<img id="c9_14" src="img1/0.jpg">
		<img id="c9_15" src="img1/0.jpg">
		<img id="c9_16" src="img1/0.jpg">
		<img id="c9_17" src="img1/0.jpg">
		<img id="c9_18" src="img1/0.jpg">
		<img id="c9_19" src="img1/0.jpg">
		<br>
	</p>
	
	<div id="gameover" onClick="offGameOver()">
		<h1 id="go"><img src="img2/gameover.jpg"></img></h1>
	</div>	
	
	<div id="win" onClick="offWin()">
		<h1 id="wi"><img src="img2/coppa.jpg"></img></h1>
	</div>	
	
	
<p id="btntouch">

<img id="btnsu" src="img2/su.jpg"></img> <br>
<img id="btnsx" src="img2/sx.jpg"></img>
<img id="btngiu" src="img2/giu.jpg"></img> 
<img id="btndx" src="img2/dx.jpg"></img> <br>
</p>

<br>

<input type="submit" id="livello1" onClick="disegnaLivello1()" class="btn btn-dark btn-lg" value="LIVELLO1">
<input type="submit" id="livello2" onClick="disegnaLivello2()" class="btn btn-dark btn-lg" value="LIVELLO2">
<input type="submit" id="livello3" onClick="disegnaLivello3()" class="btn btn-dark btn-lg" value="LIVELLO3">
<input type="submit" id="livello4" onClick="disegnaLivello4()" class="btn btn-dark btn-lg" value="LIVELLO4">
<input type="submit" id="livello5" onClick="disegnaLivello5()" class="btn btn-dark btn-lg" value="LIVELLO5">
<input type="submit" id="livello6" onClick="disegnaLivello6()" class="btn btn-dark btn-lg" value="LIVELLO6">
<input type="submit" id="livello7" onClick="disegnaLivello7()" class="btn btn-dark btn-lg" value="LIVELLO7">
<input type="submit" id="livello8" onClick="disegnaLivello8()" class="btn btn-dark btn-lg" value="LIVELLO8">
<input type="submit" id="livello9" onClick="disegnaLivello9()" class="btn btn-dark btn-lg" value="LIVELLO9">
<input type="submit" id="livello10" onClick="disegnaLivello10()" class="btn btn-dark btn-lg" value="LIVELLO10">
<br>
<br>
<br>
<div id="personaggi">
<a> <img id="camilla"  src="img1/cd.jpg"></a>
<a> <img id="stefano"  src="img1/stefano.jpg">	</a>
<a> <img id="alessandro"  src="img1/alessandro.jpg"> </a>
<a> <img id="lilpeep"  src="img1/lilpeep.jpg"> </a>
<a> <img id="paola"  src="img1/paola.jpg"> </a>
<a> <img id="federico"  src="img1/federico.jpg"> </a>
<a> <img id="omino"  src="img1/omino.jpg"> </a>
</div>
<br><br>
<div id="successo">		
</div>

<form method="get" action="salva" id="centro">

	<input type="hidden" name="punteggio" id="punteggio" value="">
  	<input type="submit" class="btn btn-dark btn-lg" value="SALVA PUNTEGGIO" id="salva" style=display:none>
 </form>
  <br>

</body>
</html>