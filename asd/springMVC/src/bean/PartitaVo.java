package bean;

public class PartitaVo {
private int punteggio;
private Utente utente;
private Gioco gioco;
private String data;


public int getPunteggio() {
	return punteggio;
}
public void setPunteggio(int punteggio) {
	this.punteggio = punteggio;
}
public Utente getUtente() {
	return utente;
}
public void setUtente(Utente utente) {
	this.utente = utente;
}
public Gioco getGioco() {
	return gioco;
}
public void setGioco(Gioco gioco) {
	this.gioco = gioco;
}
public String getData() {
	return data;
}
public void setData(String data) {
	this.data = data;
}
}
