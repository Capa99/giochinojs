package bean.conversion;

import org.springframework.beans.BeanUtils;

import bean.Dto;
import bean.Utente;
import bean.UtenteVo;
import bean.Vo;

public class UtenteConversion implements Conversion {

	
	static public UtenteVo convert(Utente dto) {
		UtenteVo vo=new UtenteVo();
		Conversion.convert(dto,vo);
		return vo;
	}
	
	static public Utente convert(UtenteVo vo) {
		Utente dto=new Utente();
		Conversion.convert(vo,dto);
		return dto;
	}

}
