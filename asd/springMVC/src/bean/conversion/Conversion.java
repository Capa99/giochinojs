package bean.conversion;

import org.springframework.beans.BeanUtils;

import bean.Dto;
import bean.Vo;

abstract interface Conversion {
	
	static public  void convert(Dto dto,Vo vo) {		
		BeanUtils.copyProperties(dto, vo);
	}
	
	static public  void convert(Vo vo,Dto dto) {		
		BeanUtils.copyProperties(vo,dto);
	}
		
	
	
	
}
