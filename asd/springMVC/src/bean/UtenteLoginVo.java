package bean;

import java.io.Serializable;

import com.sun.istack.internal.NotNull;

public class UtenteLoginVo implements Vo {
	public String toString() {
		return "UtenteLoginVo [nickname=" + nickname + ", password=" + password + "]";
	}
	@NotNull
	private  String nickname;
	
	@NotNull
	private String password;
	

	
	
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	
	
}