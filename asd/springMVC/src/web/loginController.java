package web;



import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import bean.PartitaVo;
import bean.UtenteLoginVo;
import bean.UtenteVo;
import logica.PuntiManager;
import logica.UtenteManager;


@Controller
public class loginController  extends HttpServlet {
	@GetMapping("/login2")
    public String loginView(@RequestParam("nickname") String nickname,Model model,@RequestParam("password") String password, HttpSession session) throws Exception {
		UtenteLoginVo user=new UtenteLoginVo();
		user.setNickname(nickname);
		user.setPassword(password);
		UtenteManager m=new UtenteManager();
		UtenteVo vo=m.login(user);
		if(vo!=null) {	
			model.addAttribute("utente",vo);
			session.setAttribute("user", vo);
			return "index" ;
		}
		else {
			model.addAttribute("messaggio","Password errata o nickname inesistente!");
			return "login";
			
		}
       
    }

	
	@GetMapping("/registra2")
    
    
	public String registratiView
			(@RequestParam("nickname") String nickname,
    		@RequestParam("password") String password,
    		@RequestParam("paese") String paese,@RequestParam("nome") String nome,
    		@RequestParam("cognome") String cognome,Model model) throws Exception {
		
		System.out.println("Pagina richiesta: Registrazione!");
		UtenteVo user = new UtenteVo();
		user.setNickname(nickname);
		user.setPassword(password);
		user.setPaese(paese);
		user.setNome(nome);
		user.setCognome(cognome);
		UtenteManager m=new UtenteManager();
		UtenteVo vo=m.register(user);
		if(vo!=null) {	
			model.addAttribute("utente",vo);
			return "login" ;
		}
		else {
			model.addAttribute("messaggio","errore, nickname gi� esistente");
			return "form";
			
		}   
}
	
	@GetMapping("/logout")
    public String logout(HttpSession session) {
		session.invalidate();
        return "index";
    }
        	
        	@GetMapping("/gioco")
            public String giocoView(Model model,HttpSession session) {
                UtenteVo vo=(UtenteVo) session.getAttribute("user");
                if(vo==null) {
                	model.addAttribute("messaggio","prima di giocare devi loggarti o registrarti");
                	return "index";
                }
                return "gioco";
            }
        	
        	
        	@GetMapping("/modifica")
            public String modificaView(Model model,HttpSession session) {
        		UtenteVo vo=(UtenteVo) session.getAttribute("user");
        		if(vo!=null) {
        			return "modifica";
        		}
        			model.addAttribute("messaggio","prima di modificare devi loggare");
        			return "login";              
            }
        	
        	@GetMapping("/modifica2")
            public String modificaEffettivaView(@RequestParam("nickname") String nickname,
            		@RequestParam("password") String password,
            		@RequestParam("paese") String paese,Model model,HttpSession session) throws Exception {
        		
        		System.out.println("Pagina richiesta: modifica!");
        		UtenteVo user = (UtenteVo) session.getAttribute("user");
        		user.setNickname(nickname);
        		user.setPassword(password);
        		user.setPaese(paese);
        		UtenteManager m=new UtenteManager();
        		m.modify(user);
        		model.addAttribute("messaggio","errore, nickname gi� esistente");
        			return "index";
        			
        		}  
        	
	
	
        	@GetMapping("/classifica")
            public String classificaView(Model model,HttpSession session) throws Exception {
        		UtenteVo vo=(UtenteVo) session.getAttribute("user");
        		if(vo==null) {
        			model.addAttribute("messaggio","Per visualizzare la classifica devi loggare");
        			return "login";
        		}
        		PuntiManager pm=new PuntiManager();
        		List<PartitaVo> classifica=(List<PartitaVo>) pm.classifica();
        		
        		
        		for(int i=0;i<classifica.size();i++) {
        			System.out.println(classifica.get(i).getPunteggio()+classifica.get(i).getUtente().getNickname());
        			model.addAttribute("u"+i, classifica.get(i));
        		}
                return "classifica";
            }
        	
        	@GetMapping("/index")
            public String indexView(Model model,HttpSession session) throws Exception {
        		System.out.println("Pagina richiesta: Index");
                return "index";
            }
        	
        	@GetMapping("/login")
            public String loginView(Model model,HttpSession session) {
                if(session.getAttribute("user")==null) {
                	return "login";
                }
                model.addAttribute("messaggio","Sei gia loggato");
                return "index";
            }
        	
        	@GetMapping("/registra")
            public String registraView(Model model,HttpSession session) {
                
                return "form";
            }
        	
        	@GetMapping("/salva")
            public String salvaView(Model model,@RequestParam("punteggio") int punteggio,HttpSession session) throws Exception {
                model.addAttribute("punteggio",punteggio);
                UtenteVo user = (UtenteVo) session.getAttribute("user");
        		PuntiManager m=new PuntiManager();
        		m.salvaPunteggio(punteggio, user);
        		model.addAttribute("salvato","Il tuo punteggio � stato salvato con successo");
                return "index";
            }
    }

