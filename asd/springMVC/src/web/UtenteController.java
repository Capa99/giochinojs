/*package web;

import java.sql.SQLException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import bean.ClassificaVO;
import bean.Controllo;
import bean.UtenteVO;
import logica.PuntiManager;
import logica.UtenteManager;

@Controller
public class UtenteController {

	@GetMapping("/register")
    public String registratiView(@RequestParam("user") String username,
    		@RequestParam("nome") String nome, 
    		@RequestParam("cognome") String cognome, 
    		@RequestParam("pwd") String password, Model model) {
		
		System.out.println("Pagina richiesta: Registrati");
		
		UtenteManager um = new UtenteManager();
		UtenteVo user = new UtenteVO();
		user.setUsername(username);
		user.setNome(nome);
		user.setCognome(cognome);
		user.setPassword(password);
		Controllo controllo = null;
		
		try {
			controllo = um.register(user);
		} catch (SQLException e) {
			//controllo = new Controllo();
		}
		
		model.addAttribute("controllo", controllo);
        return controllo.getUri();
    }
	
	@GetMapping("/auth")
    public String authView(@RequestParam("pwd") String pass, @RequestParam("user") String user, Model model) {
		System.out.println("Pagina richiesta: auth1" + user);
		model.addAttribute("nome", user);
		
		UtenteManager um = new UtenteManager();
		Controllo controllo = null;
		try {
			controllo = um.login(pass, user);
		} catch (SQLException e) {
			//controllo = new Controllo();
		}
		model.addAttribute("controllo", controllo);
        return controllo.getUri();
        
    }
	
	@GetMapping("/save")
	public String saveView(@RequestParam("punti") String punti, @RequestParam("nm") String nm, Model model) {
		System.out.println("Pagina richiesta: save");
		PuntiManager pm = new PuntiManager();
		long punti2 = Long.valueOf(punti).longValue();
		pm.salvaClassifica(nm, punti2);
		return "classifica";
	}
}*/
