package logica;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import bean.Gioco;
import bean.Partita;
import bean.PartitaVo;
import bean.Utente;
import bean.UtenteVo;
import bean.conversion.UtenteConversion;
import dao.GiocoHDao;
import dao.PartitaHDao;

//import org.springframework.beans.BeanUtils;

//import com.mchange.v2.codegen.bean.BeangenUtils;
/*
import bean.ClassificaVO;
import bean.StatisticaVO;
import dao.HGiocoDao;
import dao.HStatisticaDao;
import dao.HUtenteDao;
import dto.Statistica;

/**
 * Classe PuntiManager:
 * permete la manipolazione del punteggio.
 * 
 * @author Luca
 * @version 1.0
 */
public class PuntiManager {
	
	
	public List <?> classifica() throws Exception  {
		GiocoHDao giocoDao=new GiocoHDao();
	List<Partita> lista=(List<Partita>) giocoDao.getClassifica(2);
	List<PartitaVo> classifica = new ArrayList<>();
	
		for (int i=0; i<lista.size();i++) {
			classifica.add(new PartitaVo());
			classifica.get(i).setPunteggio(lista.get(i).getPunteggio());
			classifica.get(i).setUtente(lista.get(i).getUtente());
			System.out.println(classifica.get(i).getPunteggio());
		}
	
		
		return classifica;
		
	}
	
	

	public void salvaPunteggio(int punteggio,UtenteVo utente) throws Exception {

		Partita p=new Partita();
		PartitaHDao dao=new PartitaHDao();
		
		GiocoHDao gdao=new GiocoHDao();
		Gioco g=gdao.get(2);
		PartitaVo vo=new PartitaVo();
		Utente user=UtenteConversion.convert(utente);
		System.out.println(user.getId());
		p.setUtente(user);
		p.setGioco(g);
		p.setPunteggio(punteggio);
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
		p.setData(timeStamp);
		dao.insert(p);
		
		
		
	}
	
	/**
	 * Restituisce una lista di classifica di tipo VO.
	 *
	 * @return una lista di oggetti VO
	 
	public ClassificaVO getAll(){
		/*HStatisticaDao statisticaDao = new HStatisticaDao();
		Collection<Statistica> lista = statisticaDao.getAll();*/
		/*ClassificaVO classifica = new ClassificaVO();
		Collection<Statistica> lista = getStat();
		classifica.setGioco("Gioco");
		Collection<StatisticaVO> listaVO = new ArrayList<StatisticaVO>();
		for(Statistica dto : lista) {
			StatisticaVO vo = converti(dto);
			listaVO.add(vo);   
		}
		classifica.setPunteggi(listaVO);
		return classifica;
	}
	
	
	/**
	 * Ritorna una lista con la classifica.
	 *
	 * @return una lista di Classifica
	 */
	/*private Collection<Statistica> getStat() {
		HStatisticaDao statisticaDao = new HStatisticaDao();
		ArrayList<Statistica> lista = statisticaDao.getAll();
		ArrayList<Statistica> ordinata= new ArrayList<Statistica>();
		return lista;
		
	}
	
	/**
	 * Converte un oggetto DTO in uno VO
	 *
	 * @param un oggetto DTO
	 * @return un oggetto VO
	 */
	/*private StatisticaVO converti(Statistica dto) {
		StatisticaVO vo = new StatisticaVO();
		//BeanUtils.copyProperties(dto, vo);
		return vo;
	}*/
	
}
