package logica;

import org.springframework.beans.BeanUtils;
import bean.Utente;
import bean.UtenteLoginVo;
import bean.UtenteVo;
import bean.conversion.UtenteConversion;
import dao.UtenteHDao;


/**
 * Classe UtenteManager:
 * permette la gestione degli utenti.
 * 
 * @author Luca
 * @version 1.0
 */
public class UtenteManager {

	/**
	 * Controlla il possibile login per il giocatore
	 *
	 * @param password del giocatore
	 * @param username del giocatore
	 * @return controllo e messaggio di errore
	 * @throws Exception 
	 */
public UtenteVo login(UtenteLoginVo user) throws Exception{
		
		UtenteHDao utenteDao = new UtenteHDao();
		Utente u=utenteDao.getUtente(user.getNickname());
		
		
		
		if(u!= null) {
			if (u.getNickname().equals(user.getNickname()) 
			    && u.getPassword().equals(user.getPassword())) {
					return UtenteConversion.convert(u);
					
			}
		}
		
		return null;
	}
	
	/**
	 * Controlla e registra un giocatore.
	 *
	 * @param utente VO
	 * @return controllo e messaggio di errore
	 * @throws Exception 
	 **/
	
	public UtenteVo register(UtenteVo user) throws Exception {
		UtenteHDao utenteDao = new UtenteHDao();	
		Utente u=UtenteConversion.convert(user);
		if(utenteDao.getUtente(user.getNickname()) != null) {
			return null;
		}
		else {
		utenteDao.insert(u);
		return user;
		}
	}
	
	
	public UtenteVo modify(UtenteVo user) throws Exception {
		UtenteHDao utenteDao = new UtenteHDao();
		Utente u=UtenteConversion.convert(user);
	//	 u=utenteDao.getUtente(user.getNickname());
		//if(u.getPassword()==user.getPassword()){
			//if(user.getNickname()!=null) {
				if(utenteDao.getUtente(user.getNickname()) == null) {
					utenteDao.update(u, u.getId());
					return user;
				}
		//	}
		//}
					return user;
	}
}
	


